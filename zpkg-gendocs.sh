#!/bin/sh
set -o errexit
set -o nounset

./zpkg-clean.sh
python3 `which sphinx-build` -b html . _build/html
