import os,sys,struct,numpy,string,math
import SigPyProcConfig as conf

class Header:
    def __init__(self,indata):
        self.file  = None
        
        if isinstance(indata,dict):
            self.header = indata
        elif os.path.isfile(indata):
            self.header = self._header_from_file(indata)
            self.file = indata
        else:
            print "Input type not understood"
        
        self.basename = self.file.strip(".fil")
        self.original_header = self.header.copy()
        self.info = {}
        self._get_info()

    def modify_header(self,key,value):
        if key in conf.header_keys.keys():
            self.header[key] = value 
            self._get_info()
        else:
            print "key data type undefined"

    def remove_from_header(self,key):
        del self.header[key]

    def reset_header(self):
        self.header = self.original_header.copy()
        self._get_info()

    def dedisperse_header(self,DM):
        self.modify_header("refdm",DM)

    def downsample_header(self,tfactor=1,ffactor=1):
        self.modify_header("tsamp",self.header["tsamp"]*tfactor)
        self.modify_header("nchans",self.header["nchans"]/ffactor)
        self.modify_header("foff",self.header["foff"]*ffactor)
        
    def add_to_mjd(self,time_in_secs):
        if not self.header['tstart'] == 0.0:
            val = self.header['tstart']+(time_in_secs/86400.)
            self.modify_header("tstart",val)

    def make_inf(self,outfile=None):
         inf = (" Data file name without suffix          =  %s"%(self.basename),
                " Telescope used                         =  Effelsberg",
                " Instrument used                        =  PFFTS",
                " Object being observed                  =  %s"%(self.info["source_name"]),
                " J2000 Right Ascension (hh:mm:ss.ssss)  =  %s"%(self._reformat_radec(self.info["src_raj"])),
                " J2000 Declination     (dd:mm:ss.ssss)  =  %s"%(self._reformat_radec(self.info["src_dej"])),
                " Data observed by                       =  Marina Berezina",
                " Epoch of observation (MJD)             =  %.09f"%(self.info["tstart"]),
                " Barycentered?           (1=yes, 0=no)  =  %d"%(self.info.get("barycentric",0)),
                " Number of bins in the time series      =  %d"%(self.info["nsamples"]),     
                " Width of each time series bin (sec)    =  %.17gf"%(self.info["tsamp"]),
                " Any breaks in the data? (1=yes, 0=no)  =  0",
                " Type of observation (EM band)          =  Radio",
                " Beam diameter (arcsec)                 =  9.22",
                " Dispersion measure (cm-3 pc)           =  %.03f"%(self.info.get("refdm",0)),
                " Central freq of low channel (Mhz)      =  %.05f"%(self.info.get("fbottom",self.info["fch1"])),
                " Total bandwidth (Mhz)                  =  %.05f"%(self.info.get("bandwidth",0)),
                " Number of channels                     =  %d"%(self.info.get("nchans",1)),
                " Channel bandwidth (Mhz)                =  %.09f"%(abs(self.info.get("foff",0))),
                " Data analyzed by                       =  Marina Berezina",
                " Any additional notes:",
                "    Input filterbank samples have %d bits."%(self.info["nbits"]))
         
         if outfile==None:
             for line in inf:
                 print line
         else:
             f = open(outfile,"w+")
             for line in inf:
                 print >>f,line
             f.close()
                 
                
    def display(self):
        self._get_info()
        self._get_formatted_keys()

        for key in self.info.keys():
            if key in ["src_dej","src_raj"]:
                print "%s %s"%(self.formatted_keys[key],self._reformat_radec(self.info[key]))
            elif key == "telescope_id":
                print "%s %s"%(self.formatted_keys[key],conf.ids_to_telescope[self.info[key]])
            elif key == "machine_id":
                print "%s %s"%(self.formatted_keys[key],conf.ids_to_machine[self.info[key]])
            elif key == "data_type":
                if self.info[key] in conf.data_types.keys():
                    print "%s %s"%(self.formatted_keys[key],conf.data_types[self.info[key]])
                else:
                    print "%s Unknown"%(self.formatted_keys[key])
            else:
                print "%s %s"%(self.formatted_keys[key],self.info[key])

    def write_header(self):
        hstart  = "HEADER_START"
        hend    = "HEADER_END"
        header  = "".join([struct.pack("I",len(hstart)),hstart])
        for key in self.header.keys():
            if conf.header_keys[key] == "str":
                header = "".join([header,self._write_string(key,self.header[key])])
            elif conf.header_keys[key] == "I":
                header = "".join([header,self._write_int(key,self.header[key])])
            elif conf.header_keys[key] == "d":
                header = "".join([header,self._write_double(key,self.header[key])])
            elif conf.header_keys[key] == "b":
                header = "".join([header,self._write_char(key,self.header[key])])
        return "".join([header,struct.pack("I",len(hend)),hend])

    def _header_from_file(self,file):
        self._f = open(file,"r")
        header = {}
        while True:
            keylen = struct.unpack("I",self._f.read(4))[0]
            key = self._f.read(keylen)
            if not key in conf.header_keys: 
                print "'%s' not recognised header key"%(key)
                return None
            
            if conf.header_keys[key] == "str":
                value = self._read_string()
                header[key] = value
            elif conf.header_keys[key] == "I":
                value = self._read_int()
                header[key] = value
            elif conf.header_keys[key] == "b":
                value = self._read_char()
                header[key] = value
            elif conf.header_keys[key] == "d":
                value = self._read_double()
                header[key] = value
            
            if key == "HEADER_END":
                break

        self.hdrlen = self._f.tell()
        self._f.seek(0,2)
        self.filelen = self._f.tell()
        self._f.close()
        return header

    def _read_char(self):
        return struct.unpack("b",self._f.read(1))[0]

    def _read_string(self):
        strlen = struct.unpack("I",self._f.read(4))[0]
        return self._f.read(strlen)
    
    def _read_int(self):
        return struct.unpack("I",self._f.read(4))[0]

    def _read_double(self):
        return struct.unpack("d",self._f.read(8))[0]
    
    def _write_string(self,key,value):
        return "".join([struct.pack("I", len(key)),
                        key,struct.pack('I',len(value)),
                        value])

    def _write_int(self,key,value):
        return "".join([struct.pack('I',len(key)),
                        key,struct.pack('I',value)])

    def _write_double(self,key,value):
        return "".join([struct.pack('I',len(key)),
                        key,struct.pack('d',value)])
    def _write_char(self,key,value):
        return "".join([struct.pack('I',len(key)),
                        key,struct.pack('b',value)])
    def _get_info(self):
        
        for key,val in zip(self.header.keys(),self.header.values()):
            self.info[key] = val
        
        self.info['orig_hdrlen']    = self.hdrlen
        self.info['new_hdrlen']     = len(self.write_header())
        self.info['date']           = "test_string"#utils.MJD_to_Gregorian(self.header["tstart"])
        if self.header['data_type'] == 1:
            self.info['bandwidth']  = abs(self.header['foff'])*self.header['nchans']
            self.info['fbottom']    = self.header['fch1']-abs(self.info['bandwidth'])

        if self.file: 
            self.info['filename']   = self.file

            self.info['nsamples']   = ((os.path.getsize(self.file)-self.hdrlen)
                                       /self.header['nchans'])*8/self.header['nbits']

    def _reformat_radec(self,val):
        if val < 0:
            sign = -1
        else:
            sign = 1

        fractional,integral = math.modf(abs(val))
        xx = (integral-(integral%10000))/10000
        yy = ((integral-(integral%100))/100)-xx*100
        zz = integral - 100*yy - 10000*xx + fractional
        zz = string.zfill("%.4f"%(zz),7)
        return "%02d:%02d:%s"%(sign*xx,yy,zz)

    def _get_formatted_keys(self):
        combi_keys = self.header.keys()+self.info.keys()
        maxstrlen = len(max(combi_keys,key=len))+\
            len(max(conf.header_units.keys(), key=len))+3
        
        formatted_keys = {}
        
        for key in combi_keys:
            prstr = "%s %s"%(key,conf.header_units[key]) 
            prstr ="".join([prstr," "*(maxstrlen-len(prstr))]) 
            formatted_keys[key] = "%s:"%(prstr)
        self.formatted_keys = formatted_keys
                
    def npheader(self):
        dtypes = [(key,conf.struct_to_numpy[dtype]) for key,dtype\
                      in zip(conf.header_keys.keys(),conf.header_keys.values())\
                      if not dtype in [None,"b"]]

        array = numpy.empty(shape=[1],dtype=dtypes)
        for key in self.header.keys():
            array[key] = self.header[key]
        for key in self.info.keys():
            array[key] = self.info[key]
        return(array)


class MultiHeader:
    def __init__(self,files):
        assert not isinstance(files, str)
        self.files = files
        self.header_dict = {}
        self.info = {}
        for file in files:
            self.header_dict[file] = Header(file)
        for file in files:
            self.info[file] = self.header_dict[file].info

        self.header_array = numpy.concatenate([self.header_dict[key].npheader() for key in self.header_dict.keys()])
        self._bin_offsets()
        #self._map_channels()

    def min_attribute(self,attribute):
        return min(self.header_array[attribute])

    def test_attribute(self,attribute,mode="same"):
        if mode == "same":
            return(len(set(self.header_array[attribute])) == 1)
        elif mode == "different":
            return(len(set(self.header_array[attribute])) == len(self.header_array[attribute]))

    def _map_channels(self,use_offsets=True):

        # This is a horrible peice of code
        if not self.test_attribute("foff",mode="same"):
            return(None)
        if not self.test_attribute("fch1",mode="different"):
            return(None)
        
        foff = abs(self.header_array['foff'][0])
        ftop = self.header_array['fch1'].max()
        fbottom = self.header_array['fbottom'].min()+foff
        sorted_array = numpy.sort(self.header_array,order="fch1")[::-1]
        channels = {}
        count = 0
        while True:
            channel_freq = ftop-(count*foff)
            for array in sorted_array:
                if channel_freq <= array["fch1"] and channel_freq > array["fbottom"]:
                    filechan = int(array["nchans"] - round((channel_freq-array["fbottom"]+foff)/foff))
                    channels[count] = [array["filename"],filechan,self.offsets[array["filename"]]]
                    break
            else:
                if channel_freq<fbottom:
                    break
                else:
                    channels[count] = [None,None,None]
            count+=1
        self.channel_map = channels

    def _bin_offsets(self):
        if not self.test_attribute("tsamp",mode="same"):
            return(None)
        self.offsets = {}
        absstart = self.header_array["tstart"].min()
        tsamp = self.header_array[0]["tsamp"]
        for file,tstart in zip(self.files,self.header_array["tstart"]):
            self.offsets[file] = int(round((tstart-absstart)*86400./tsamp))
