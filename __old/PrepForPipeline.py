import os,sys,Header,Masking,glob

INCOMING   = "/data2/FAST_PIPELINE/INCOMING/MORE/"
PENDING    = "/data2/FAST_PIPELINE/PENDING/"
PROCESSING = "/data2/FAST_PIPELINE/PROCESSING/"
COMPLETED  = "/data2/FAST_PIPELINE/COMPLETED/"
ZAPLISTS   = "/data2/FAST_PIPELINE/ZAPLISTS/"
EXTRAS     = "/data2/FAST_PIPELINE/EXTRAS/"
TOTAPE     = "/data2/survey/data_to_archive/"

class MaskHandler:
    def __init__(self,scan):
        self.scan = scan
        self.filterbanks = glob.glob("%s*_8bit.fil"%(self.scan))
        self.header = Header.Header(self.filterbanks[0])
        self.surveyname = "%s_masked_%d"%(self.header.info["source_name"].split()[0],
                                   int(self.header.info["tstart"]))
        self.zapfile = "%s.zaplist"%(self.surveyname)
        self.failure = False
        for file in glob.glob("%s*.gz"%(self.scan)):
            os.system("gunzip %s"%(file))
        print "Processing scan %s with zaplist name %s"%(self.scan,self.zapfile)

    def Mask(self):
        #try:
        Masking.main(self.scan)
        #except:
        #    print "Failure detected in masking of scan %s"%(self.scan)
        #    self.failure = True
        
        #os.system("/data1/masterscripts/SigPyProc/Masking.py %s"%(self.scan))
        #sys.exit()

        if not self.failure: 
            os.system("cp %s.zaplist %s%s.zaplist"%(self.scan,ZAPLISTS,self.surveyname))
            self.maskedfils = glob.glob("%s*_8bit_masked.fil"%(self.scan))
            for fil in self.maskedfils:
                os.system("mv %s %s"%(fil,PENDING))
        
            os.system("rm -rf *.dat *.inf *.fft")
            
            for file in glob.glob("%s*mask"%(self.scan)):
                os.system("gzip %s"%(file))
            
            os.system("mv %s*.MBmask.gz %s*.offsets %s*.zaplist %s*.mask.gz %s*.fil %s*.maxmin %s*.badchans %s"%\
                          (self.scan,self.scan,self.scan,self.scan,self.scan,self.scan,self.scan,TOTAPE))
            
            

def getScans():
    all_files = glob.glob("*.fil")
    scans = []
    for file in all_files:
        scans.append(file.strip()[:4])
    return list(set(scans))      


def main():
    os.chdir(INCOMING)
    scans = getScans()
    print "Found %d scans to process..."%(len(scans))
    for scan in scans:
        M = MaskHandler(scan)
        M.Mask()
    
main()
