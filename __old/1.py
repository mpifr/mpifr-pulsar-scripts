############################################################
# Ewan Barr
# GUI for general data management on the panoramix computer
############################################################

import os,sys,numpy,threading,time,datetime
import Tkinter as tk
import Masking,Header
from commands import getoutput as getout

TEXTOPTS = {"width":90,
            "height":20,
            "bg":"black",
            "fg":"green"}




ENTRYOPTS = {"width":50,
             "bg":"white",
             "fg":"black"}

LABELOPTS = {"bg":"black",
             "fg":"lightblue"}

NBUTTONOPTS = {"bg":"lightblue",
              "fg":"black"}

RBUTTONOPTS = {"bg":"red", 
               "fg":"black"}
#on psrfbs
CONVERTER = "/home/pulsar/scripts/new421_makemaskscode.py"

USER = "pulsar"

HOSTS = {"psrfb0":0,
         "psrfb1":1,
         "psrfb2":2,
         "psrfb3":3,
         "psrfb4":4,
         "psrfb5":5,
         "psrfb6":6}

class BaseWindow:
    def __init__(self,root):
        self.root = root

        self.protokollVar = tk.StringVar()
        self.dumpVar      = tk.StringVar()
        self.protokollVar.set("/data2/survey/protokoll_logs/")
        self.dumpVar.set("/data1/FAST_PIPELINE/INCOMING/")
        
        self.transferFrame = tk.Frame(self.root)
        self.transferFrame.grid(row=0,column=0)        
        
        self.transferText = tk.Text(self.transferFrame,TEXTOPTS)
        self.transferText.grid(row=0,column=0,columnspan=2,pady=5)
        self.transferText.config(state="disabled")
        
        self.protokollLabel = tk.Label(self.transferFrame,LABELOPTS,text="Protokoll log:")
        self.protokollLabel.grid(row=1,column=0)
        
        self.protokollEntry = tk.Entry(self.transferFrame,ENTRYOPTS,textvariable=self.protokollVar)
        self.protokollEntry.grid(row=1,column=1)
        
        self.dumpLabel = tk.Label(self.transferFrame,LABELOPTS,text="Target directory:")
        self.dumpLabel.grid(row=2,column=0)
        
        self.dumpEntry = tk.Entry(self.transferFrame,ENTRYOPTS,textvariable=self.dumpVar)
        self.dumpEntry.grid(row=2,column=1)
        
        self.transferButton = tk.Button(self.transferFrame,NBUTTONOPTS,
                                    text="Transfer data",
                                    command=self.runTransfer)
        self.transferButton.grid(row=3,column=0,columnspan=2,pady=5)
        
        self.SStop = tk.Button(self.transferFrame,NBUTTONOPTS,
                               text="Soft stop",
                               command=self.softStop)
        self.SStop.grid(row=4,column=0,columnspan=2,pady=5)
        self.SStop.config(state="disabled",relief="sunken")

        self.HStop = tk.Button(self.transferFrame,RBUTTONOPTS,
                               text="Hard stop",
                               command=self.hardStop)
        self.HStop.grid(row=5,column=0,columnspan=2,pady=5)
        self.HStop.config(state="disabled",relief="sunken")
        
    def hardStop(self):
        self.write("Performing hard stop...\n")
        self.transApp.stop = True
        for host in HOSTS.keys():
            self.write("Killing related processes on %s\n"%(host))
            cmd = "ssh %s@%s ps -aef | egrep 'python|421.exe|filterbankPFFTS' | awk '{print$2}'"%\
                (USER,host)
            pids = getout(cmd).split()
            for pid in pids:
                cmd = "ssh %s@%s kill -9 %s "%\
                    (USER,host,pid)
            os.system(cmd)
        self.transferButton.config(state="normal",relief="raised")


    def softStop(self):
        self.write("Performing soft stop...\n")
        self.transApp.stop = True
        self.transferButton.config(state="normal",relief="raised")
        
    def runTransfer(self):
        self.transferButton.config(state="disabled",relief="sunken")
        self.SStop.config(state="normal",relief="raised")
        self.HStop.config(state="normal",relief="raised")
        self.transApp = DataTransfer(self)
        if not self.transApp.stop:
            self.transApp.start()
            
    def write(self,str):
        time = datetime.datetime.now().strftime("%d/%m/%Y %H:%M -->")
        self.transferText.config(state="normal")
        self.transferText.insert("end","%s %s"%(time,str))
        self.transferText.config(state="disabled")


class DataTransfer(threading.Thread):
    def __init__(self,GUI):
        threading.Thread.__init__(self)
        self.stop   = False
        self.dump   = GUI.dumpVar.get()
        self.log    = GUI.protokollVar.get()
        self.GUI    = GUI 
        
        if not os.path.isdir(self.dump):
            self.GUI.write("Target directory not found...\n")
            self.stop = True
        elif not os.path.isfile(self.log):
            self.GUI.write("Protokoll log not found...\n")
            self.stop = True

        if not self.stop:
            self.GUI.write("Moving to directory %s\n"%(self.dump))
            os.chdir(self.dump)
      
            self.GUI.write("Parsing protokoll log %s\n"%(self.log))
            self.scans = self.parseLogs()
            self.GUI.write("%d pointings found...\n"%(len(self.scans)))

    def run(self):
        for scan in self.scans :
            skip = True
            for beam in HOSTS.values():
                filfile    = scan+"_0001_0"+str(beam)+"_8bit.fil"
                mask       = scan+"_0001_0"+str(beam)+"_8bit.mask.gz"
                bpassmask  = scan+"_0001_0"+str(beam)+"_8bit.badchans"
                maxmin     = scan+"_0001_0"+str(beam)+"_8bit.maxmin"
                for file in [filfile,mask,bpassmask,maxmin]:
                    if not os.path.isfile("%s/%s"%(self.dump,file)):
                        skip = False

            if self.stop:
                skip = True

            if not skip:
                self.bitconvertCopy(scan)
                self.GUI.write("All beams returned for scan %s\n"%(scan))
                 
        self.GUI.write("All scans completed...\n")
        self.GUI.transApp.stop = True
        self.GUI.transferButton.config(state="normal",relief="raised")
        self.GUI.HStop.config(state="disabled",relief="sunken")
        self.GUI.SStop.config(state="disabled",relief="sunken")

    def bitconvertCopy(self,scan):
        
        for host,beam in zip(HOSTS.keys(),HOSTS.values()):
            end_file = "%s_%d_complete"%(scan,beam)
            cmd = "scp %s@%s:/data2/dump/survey/%s ."%\
                        (USER,host,end_file)
            try:
                error = getout(cmd)
            except:pass

            if not os.path.isfile(end_file):
                cmd = "ssh %s@%s python %s %s %d &"%\
                    (USER,host,CONVERTER,scan,beam)
                os.system(cmd)

        while True :
            if self.stop:
                break
            complete = 0
            for host,beam in zip(HOSTS.keys(),HOSTS.values()):
                end_file = "%s_%d_complete"%(scan,beam)
                if not os.path.isfile(end_file):
                    cmd = "scp %s@%s:/data2/dump/survey/%s ."%\
                        (USER,host,end_file)
                    try:
                        error = getout(cmd)
                    except:pass
                    
                    
                if os.path.isfile(end_file):
                    complete += 1
                
            if complete == len(HOSTS) :
                break


        if self.stop:
            return None

        for host,beam in zip(HOSTS.keys(),HOSTS.values()):
            end_file = "%s_%d_complete"%(scan,beam)
            os.remove(end_file)
            filfile    = scan+"_0001_0"+str(beam)+"_8bit.fil"
            mask       = scan+"_0001_0"+str(beam)+"_8bit.mask.gz"
            bpassmask  = scan+"_0001_0"+str(beam)+"_8bit.badchans"
            maxmin     = scan+"_0001_0"+str(beam)+"_8bit.maxmin"
            for file in [filfile,mask,bpassmask,maxmin]:
                cmd = "scp %s@%s:/data2/dump/survey/%s %s"%\
                    (USER,host,file,self.dump)
                os.system(cmd)

    def parseLogs(self):
        

        f = open(self.log,"r")
        lines = f.readlines()
        f.close()
        scans = [i.split()[0] for i in lines]
        """
        for line in lines:
            if not len(line.split()) >= 15:
                continue
            elif line.split()[0] == "Scan":
                continue
            else:
                source = line.split()[4]
                if source.strip()[:1] in ["G","Z"]:
                    scan = line.split()[1]
                    scans.append(scan)
        """
        return scans

root = tk.Tk()
root.title("Panoramix survey control")
root.tk_setPalette("black")
BaseWindow(root)
root.mainloop()
