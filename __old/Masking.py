import numpy,sys,glob,time,pylab,os,cPickle
from commands import getoutput as getout
from scipy.signal import correlate,medfilt







import Header,FilMethods
from decimal import Decimal as D

WINDOW = 200                # Size of correlation window (should be bigger than biggest expected lag ~60)
BEAMTHRESH = 4              # Number of coincidence detections required for masking
CHANTHRESH = 0.001          # Fraction of bad samples in a channel for that channel to be masked  
SPIKESIGMA = 2              # Sigma level for MB tim comparison for spikes
DIPSIGMA = 4                # Sigma level for MB tim comparison for spikes
FFTTHRESH = 1.5               # Sigma level for MB fft comparison for spikes
DROPOUTS = "/media/outgoing/FAST_PIPELINE/NOTES/Dropouts.txt"

class Mask(Header.Header):
    def __init__(self,filename):
        Header.Header.__init__(self,filename)
        self.basename = self.basename.strip(".mask")
        
    def getTim(self):
        print "getTim(self)"
        print self.file
        self.tim,self.bpass =  FilMethods.getTim(self.info)
        self.tim = numpy.array(self.tim)
        self.bpass = numpy.array(self.bpass)
        self.tim -= self.tim.mean()

    def offsetFromRef(self,ref):
        print "offsetFromRef(self,ref)"
        corr = correlate(ref[WINDOW:-WINDOW],self.tim,mode="valid")
        return corr.argmax()-WINDOW

    def getBadChans(self):
        print "getBadChans(self)"
        #plot(self.bpass.astype("float")/self.info["nsamples"])
        return numpy.where(self.bpass>(CHANTHRESH*self.info["nsamples"]))[0]

class Filterbank(Header.Header):
    def __init__(self,filename,offsets,MBmask):
        Header.Header.__init__(self,filename)
        self.MBmask = MBmask
        self.offset = offsets[self.basename]
        self.tim,self.bpass = FilMethods.getTim(self.info,offset=self.offset,read=MBmask.info["nsamples"])
        self.badchans = parse_bad_channels("%s.badchans"%(self.basename))
        self.tim = numpy.array(self.tim)
        self.mean = self.tim.mean()
        self.std = self.tim.std()
        self.masked_file = "%s_masked.fil"%(self.basename)
        self.testDropout()

    def testDropout(self):
        if self.tim.min() == 0:
            f = open(DROPOUTS,"a")
            print >>f,self.basename
            f.close()

    def writeDat(self):
        datfile = "%s.dat"%(self.basename)
        inffile = "%s.inf"%(self.basename)
        #for presto we write single precision float
        self.tim.astype("float32").tofile(datfile)
        self.make_inf(outfile=inffile)
        
    def applyMask(self,badsamps,extrabadchans,omask):
        print "applyMask(self,badsamps,extrabadchans)"
        self.modify_header("source_name","%s_masked"%(self.header["source_name"].split()[0]))

        self.badchans += extrabadchans.tolist()
        #here write some code to assert lists as ints (not numpy.int64)
        self.badchans = [int(chan) for chan in self.badchans]
        badsamps = [int(samp) for samp in badsamps]

        print "Number of channels to be masked: %d"%(len(self.badchans))
        print "Number of samples to be fully masked: %d"%(len(badsamps))

        FilMethods.applyMask(self.info,self.MBmask.info,omask.info,
                             self.write_header(),self.badchans,
                             badsamps,self.masked_file,self.offset)

class MaskedFil(Header.Header):
    def __init__(self,filename):
        Header.Header.__init__(self,filename)
        self.tim,self.bpass = FilMethods.getTim(self.info)
        self.tim = numpy.array(self.tim)
        if not self.tim.shape[0] % 2 == 0:
            self.tim = self.tim[:-1]
            self.info["nsamples"]-=1
        self.mean = self.tim.mean()
        self.std = self.tim.std()
        self.fftdata = None
        self.fftpows = None

    def writeDat(self):
        print"writeDat(self)"
        datfile = "%s.dat"%(self.basename)
        inffile = "%s.inf"%(self.basename)
        self.tim.astype("float32").tofile(datfile)
        self.make_inf(outfile=inffile)
        
    def getFFT(self):
        print "getFFT(self)"
        getout("realfft %s.dat"%(self.basename))
        getout("rednoise %s.fft"%(self.basename))
        getout("cp %s.inf %s_red.inf"%(self.basename,self.basename))
        data = numpy.fromfile("%s_red.fft"%(self.basename),dtype="float32")

        
        self.fftpows = (data.reshape(data.shape[0]/2,2)**2).sum(axis=1)
        self.fftstd  = self.fftpows.std()
        self.fftmean = self.fftpows.mean()
        self.fftpows = (self.fftpows-self.fftmean)/self.fftstd
        
class MultiMask(Header.MultiHeader):
    def __init__(self,files):
        Header.MultiHeader.__init__(self,files)
        self.masks = [Mask(file) for file in files]
        self.refMask = self.masks[0]
        self.compMasks = self.masks[1:]
        for mask in self.masks:
            mask.getTim()
                    
    def getOffsets(self):
        print "getOffsets(self)"
        offsets = {self.refMask.basename:0}
        refTim = self.refMask.tim
        for mask in self.compMasks:
            offsets[mask.basename] = mask.offsetFromRef(refTim)
        minlag = min(offsets.values())
        if minlag < 0:
            minlag *= -1
        for key in offsets.keys():
            offsets[key] = int(offsets[key]+minlag)
            self.info["%s.mask"%(key)]["sample_offset"] = offsets[key]
            print "Found an offset of %d for file %s"%(offsets[key],key)
        cPickle.dump(offsets,open("%s.offsets"%(self.refMask.file.split("_")[0]),"w"))
        return offsets
        
    def genMask(self):
        print "genMask(self)"
        outfile = "%s.MBmask"%(self.refMask.file.split("_")[0])
        self.refMask.modify_header("source_name","%s_maskfile"%(self.refMask.header["source_name"].split()[0]))
        FilMethods.compareMasks(self.refMask.write_header(),self.info,outfile,BEAMTHRESH)
        return Mask(outfile)

class MultiFil(Header.MultiHeader):
    def __init__(self,files,MBmask,offsets):
        Header.MultiHeader.__init__(self,files)
        self.MBmask = MBmask
        self.fils = [Filterbank(file,offsets,MBmask) for file in files]
                
    def getBadSamps(self):
        print "getBadSamps(self)"
        spikeacc = numpy.zeros(self.MBmask.info["nsamples"])
        dipacc = numpy.zeros(self.MBmask.info["nsamples"])
        for fil in self.fils:
            spikeids = numpy.where(fil.tim>(fil.mean+(fil.std*SPIKESIGMA)))
            dipids = numpy.where(fil.tim<(fil.mean-(fil.std*DIPSIGMA)))
            spikeacc[spikeids]+=1
            dipacc[dipids]+=4
            
        #plot(spikeacc)
        #plot(dipacc)

        return numpy.hstack((numpy.where(spikeacc>=BEAMTHRESH),
                             numpy.where(dipacc>=BEAMTHRESH))).ravel().tolist()

    def applyMasks(self,badchans,badsamps,omasks):
        print "applyMasks(self,badchans,badsamps)"
        for fil,omask in zip(self.fils,omasks):
            fil.applyMask(badsamps,badchans,omask)

    def writeDats(self):
        print "Writing dat and inf files"
        for fil in self.fils:
            fil.writeDat()

class MultiMaskedFil(Header.MultiHeader):
    def __init__(self,files):
        Header.MultiHeader.__init__(self,files)
        self.fils = [MaskedFil(file) for file in files]
        
    def writeDats(self):
        print "Writing dat and inf files"
        for fil in self.fils:
            fil.writeDat() 

    def getFFTs(self):
        for fil in self.fils:
            fil.getFFT()
    
    def compareFFTs(self):
        #if not self.test_attribute("nsamples","same"):
        #    return None
        self.stats = self.fils[0].info
        accu = numpy.zeros(self.stats["nsamples"]+1)
        for fil in self.fils:
            accu[numpy.where(fil.fftpows>FFTTHRESH**2)]+=1
    
        bins = numpy.where(accu>=4)[0]
        width = D(1)/(D(str(self.stats["tsamp"]))*D(self.stats["nsamples"]))
        
        
        frequencies = [D(bin)*width for bin in bins]
        zaplist = "%s.zaplist"%(self.stats["filename"].strip()[:4])
        f = open(zaplist,"w+")
        for freq in sorted(frequencies):
            factor = numpy.log10(freq)
            if factor < 1:
                # print>>f,freq,width
                print >>f,freq-width/2,freq+width/2,1,1
            else:
                #print>>f,freq,width*factor
                print>>f,freq-(width*factor)/2,freq+(width*factor)/2,1,1
        f.close()
        
def parse_bad_channels(file):
    chanlist = []
    if os.path.isfile(file):
        f = open(file,"r")
        lines = f.readlines()
        f.close()
        for line in lines:
            chanlist.append(int(line.split()[0]))
    else:
        print "Bad channels file not found!"
    return chanlist

def plot(x):
    pylab.plot(x)
    pylab.show()
    raw_input()

def main(scan):

    maskfiles = glob.glob("%s*_8bit.mask.gz"%(scan))
    for file in maskfiles:
        os.system("gunzip %s"%(file))
    maskfiles = glob.glob("%s*_8bit.mask"%(scan))
    filfiles = glob.glob("%s*_8bit.fil"%(scan))
    
    all_masks = MultiMask(maskfiles)
    offsets = all_masks.getOffsets()
    MBmask = all_masks.genMask()
    
    MBmask.getTim()

    badchans = MBmask.getBadChans()
    all_fils = MultiFil(filfiles,MBmask,offsets)
    badsamps = all_fils.getBadSamps()
    all_fils.applyMasks(badchans,badsamps,all_masks.masks)

    maskedfiles = [fil.masked_file for fil in all_fils.fils]

    #maskedfiles = glob.glob("%s*_8bit_masked.fil"%(scan))
    all_mfils = MultiMaskedFil(maskedfiles)
    all_mfils.writeDats()
    all_mfils.getFFTs()
    all_mfils.compareFFTs()

    del all_fils,all_mfils,all_masks

 
if __name__ == "__main__":
    main(sys.argv[1])
