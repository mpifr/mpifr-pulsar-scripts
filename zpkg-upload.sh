#!/bin/sh
set -o errexit
set -o nounset

python3 ./setup.py build sdist register upload
